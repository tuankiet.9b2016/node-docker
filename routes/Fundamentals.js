//Sum two numbers
function myFunction(a, b) {
    return a + b; 
}
//Comparison operators, strict equality
function myFunction(a, b) {
    return a === b;
}
//Get type of value
function myFunction(a) {
    return typeof a;
}
//Get nth character of string
function myFunction(a, n) {
    return a[n - 1];
}
//Get first n character of string
function myFunction(a) {
    return a.slice(0, 3);
}
//Find the position of one string in another
function myFunction(a) {
    return a.indexOf('is');
}
//Extract the first half of a string
function myFunction(a) {
    return a.slice(0, a.length / 2);
}
//Remove last n characters of string
function myFunction(a) {
    return a.slice(0, -3);
}
//Return the percentage of a number
function myFunction(a, b) {
    return b / 100 * a
}
//Basic JavaScript math operators
function myFunction(a, b, c, d, e, f) {
    return (((a + b - c) * d) / e) ** f;
 }
//Check whether a string contains another string and concatenate
function myFunction(a, b){
    if(a.includes(b)) {
    return b.concat(a)
    } else {
    return a.concat(b)
    }
}
//Check if a number is even
function myFunction(a) {
    if(a % 2 ==0){
        return true;
    } else {
        return false;
    }
}
//How many times does a character occur?
function myFunction(a,b) {
    var count = 0;
    for(var i=0; i<b.length; i++)
    {
        if(b.charAt(i) == a) {
            count++;   
        }
    }
    return count++;
}
//Check if a number is a whole number
function myFunction(a){
    return a % 1 == 0 ? true : false;
}
//Multiplication, division, and comparison operators
function myFunction(a,b) {
    return a < b ? a / b : a*b;
}
//Round a number to 2 decimal places
function myFunction(a) {
    return Number(a.toFixed(2));
  }
  //Split a number into its digits
  function myFunction( a ) {
    const string = a + '';
    const strings = string.split('');
    return strings.map(digit => Number(digit))
  }